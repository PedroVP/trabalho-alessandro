#ifndef NODO_H
#define NODO_H
template <typename T>
class nodo
{
    public:
        nodo();
        virtual ~nodo();
        void setValor(T);
        void setProximo(nodo*);
        T getValor();
        nodo* getProximo();

    private:
        T valor;
        nodo* proximo;
};

#endif // NODO_H
