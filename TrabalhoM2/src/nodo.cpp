#include "nodo.h"
template <typename T>
nodo<T>::nodo()
{
    //ctor
}
template <typename T>
nodo<T>::~nodo()
{
    //dtor
}
template <typename T>
void nodo<T>::setValor(T valor)
{
    this.valor = valor;
}
template <typename T>
void nodo<T>::setProximo(nodo* proximo)
{
    this.proximo = proximo;
}
template <typename T>
T nodo<T>::getValor()
{
    return valor;
}
template <typename T>
nodo<T>* nodo<T>::getProximo()
{
    return *proximo;
}
