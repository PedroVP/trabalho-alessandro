#include <iostream>
#include "MatrizEsparsa.h"

using namespace std;

MatrizEsparsa::MatrizEsparsa(int linha, int coluna)
{
    qtdMaxColuna = coluna;
    qtdMaxLinha = linha;
    colunaInicio = NULL;
}

MatrizEsparsa::~MatrizEsparsa()
{
    //dtor
}

void MatrizEsparsa::adicionaElemento(int linha, int coluna, int valor)
{
    if(valor != 0)
    {
        NodoLista * aux = colunaInicio;
        if(!aux)
        {
            colunaInicio = criaColuna(NULL, linha, coluna, valor);
        }
        else if(aux->getColuna() > coluna)
        {
            colunaInicio = criaColuna(colunaInicio, linha, coluna, valor);
        }
        else
        {
            while(aux->pegaProxima() && aux->pegaProxima()->getColuna() <= coluna)
            {
                aux = aux->pegaProxima();
            }

            if(aux->getColuna() == coluna)
            {
                aux->insere(linha, valor);
            }
            else if(aux->pegaProxima())
            {
                aux->setaProxima(criaColuna(aux->pegaProxima(), linha, coluna, valor));
            }
            else
            {
                aux->setaProxima(criaColuna(NULL, linha, coluna, valor));
            }
        }
    }
}

void MatrizEsparsa::adicionaAcoluna(int coluna, int valor)
{
    NodoLista * aux = colunaInicio;

    if(!aux)
    {
        colunaInicio = criaColuna(NULL, 0, coluna, valor);
    }
    else
    {
        if(colunaInicio->getColuna() > coluna)
        {
            colunaInicio = criaColuna(aux, 0,coluna, valor);
        }

        while(aux->pegaProxima() && aux->getColuna() < coluna)
        {
            aux = aux->pegaProxima();
        }

        if(aux->getColuna() == coluna)
        {
            aux->acrescentaValor(valor);
        }
        else if(aux->pegaProxima())
        {
            aux->setaProxima(criaColuna(aux->pegaProxima(), 0, coluna, valor));
        }
        else
        {
            aux->setaProxima(criaColuna(NULL, 0, coluna, valor));
        }
    }
}

NodoLista* MatrizEsparsa::criaColuna(NodoLista* proximo, int linha,int coluna, int valor)
{
    NodoLista *novaColuna = new NodoLista(coluna);
    if(linha != 0)
    {
        novaColuna->insere(linha, valor);
    }
    else
    {
        novaColuna->acrescentaValor(valor);
    }

    novaColuna->setaProxima(proximo);
    return novaColuna;
}

void MatrizEsparsa::removeElemento(int linha, int coluna)
{
    NodoLista * aux = colunaInicio;

    while(aux && aux->getColuna() < coluna)
    {
        aux = aux->pegaProxima();
    }

    if(aux && aux->getColuna() == coluna)
    {
        aux->pegaLista()->retira(linha);
    }
}

void MatrizEsparsa::mostraMatriz()
{
    NodoLista * aux;

    for(int i = 1; i <= qtdMaxLinha; i++)
    {
        aux = colunaInicio;
        for(int j = 1; j <= qtdMaxColuna; j++)
        {
            while(aux && aux->getColuna() < j)
            {
                aux = aux->pegaProxima();
            }
            if(aux && aux->getColuna()==j)
            {
                aux->mostra(i);
            }
            else
            {
                cout << 0 << " ";
            }
        }
        cout << endl;
    }
}
