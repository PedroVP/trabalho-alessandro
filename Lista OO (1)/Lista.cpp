#include "Lista.h"
#include "Nodo.cpp"

using namespace std;

        Lista::Lista(){
            this->inicio = NULL;
        }

        Lista::~Lista(){
            Nodo* nodo;

            while(this->inicio != NULL)
            {
                nodo = this->inicio;
                this->inicio = this->inicio->pegaProximo();
                delete nodo;
            }
        }

        bool Lista::existeElemento(int elemento){
            Nodo* nodo = this->inicio;
            while (nodo != NULL){
                if (nodo->pegaElemento() == elemento)
                    return true;
                nodo = nodo->pegaProximo();
            }
            return false;
        }

        int Lista::umElemento(int linha){
            Nodo* nodo = this->inicio;

            while (nodo && nodo->getLinha() < linha){
                nodo = nodo->pegaProximo();
            }

            if(!nodo || nodo->getLinha() > linha)
            {
                return 0;
            }

            return nodo->pegaElemento();
        }

        int Lista::posicao(int elemento){
            Nodo* nodo = this->inicio;

            while (nodo->pegaElemento() != elemento){
                nodo = nodo->pegaProximo();
            }
            return nodo->getLinha();
        }

        void Lista::insere(int elemento, int linha)
        {
            Nodo* nodo = new Nodo(elemento, linha);

            if (this->inicio == NULL)
            {
                this->inicio = nodo;
            }
            else
            {
                if (linha < inicio->getLinha())
                {
                    nodo->setaProximo(this->inicio);
                    this->inicio = nodo;
                }
                else if(linha == inicio->getLinha())
                {
                    this->inicio->setaElemento(elemento);
                }
                else
                {
                    Nodo* nodoAnterior = this->inicio;

                    while (nodoAnterior->pegaProximo() && nodoAnterior->pegaProximo()->getLinha() < linha){
                        nodoAnterior = nodoAnterior->pegaProximo();
                    }
                    if(nodoAnterior->pegaProximo())
                    {
                        cout << "passou";
                        if(nodoAnterior->pegaProximo()->getLinha() != linha)
                        {
                            nodo->setaProximo(nodoAnterior->pegaProximo());
                            nodoAnterior->setaProximo(nodo);
                        }
                        else
                        {
                            nodoAnterior->pegaProximo()->setaElemento(elemento);
                        }
                    }
                    else
                    {
                        cout << "entrou";
                        nodoAnterior->setaProximo(nodo);
                    }
                }
            }
        }

        void Lista::retira(int linha){
            Nodo* nodo = this->inicio;

            if(nodo)
            {

                if(nodo->getLinha() == linha)
                {
                    inicio = inicio->pegaProximo();
                    delete nodo;
                }
                else
                {
                    Nodo* nodoAnterior = this->inicio;
                    while(nodoAnterior->pegaProximo() && nodoAnterior->pegaProximo()->getLinha() < linha)
                    {
                        nodoAnterior = nodoAnterior->pegaProximo();
                    }

                    if(nodoAnterior->pegaProximo())
                    {
                        if (linha == nodoAnterior->pegaProximo()->getLinha()){
                            nodo = nodoAnterior->pegaProximo();
                            if(nodo->pegaProximo())
                            {
                                nodoAnterior->setaProximo(nodo->pegaProximo());
                            }
                            delete nodo;
                        }
                    }
                }

            }
        }
