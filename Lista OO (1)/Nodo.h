#ifndef NODO_H_INCLUDED
#define NODO_H_INCLUDED

class Nodo{
    private:
        int elemento;
        int linha;
        Nodo* proximo;
    public:
        Nodo(int elemento, int linha);
        void setaElemento(int elemento);
        void setaProximo(Nodo* nodo);
        int getLinha();
        int pegaElemento();
        Nodo* pegaProximo();
};

#endif // NODO_H_INCLUDED
