#ifndef NODOLISTA_H
#define NODOLISTA_H

#include "Lista.h"

class NodoLista
{
    public:
        NodoLista(int coluna);
        virtual ~NodoLista();
        void setaLista(Lista*);
        void setaProxima(NodoLista*);
        Lista* pegaLista();
        NodoLista* pegaProxima();
        int getColuna();
        void acrescentaValor(int);
        void insere(int linha, int valor);
        void mostra(int linha);

    private:
        Lista* atual;
        NodoLista* proxima;
        int coluna;
        int acrescimoElementos;
};

#endif // NODOLISTA_H
