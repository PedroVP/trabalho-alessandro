#ifndef MATRIZESPARSA_H
#define MATRIZESPARSA_H

#include "NodoLista.h"

class MatrizEsparsa
{
    public:
        MatrizEsparsa(int, int);
        virtual ~MatrizEsparsa();

        void mostraMatriz();
        void adicionaElemento(int linha, int coluna, int valor);
        void adicionaAcoluna(int coluna, int valor);
        void removeElemento(int linha, int coluna);

    private:
        int qtdMaxColuna;
        int qtdMaxLinha;
        NodoLista *colunaInicio;
        NodoLista* criaColuna(NodoLista* proximo,int linha,int coluna,int valor);
};

#endif // MATRIZESPARSA_H
