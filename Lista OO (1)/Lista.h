#ifndef LISTA_H_INCLUDED
#define LISTA_H_INCLUDED

#include "Nodo.h"

class Lista {
    private:
        Nodo* inicio;
    public:
        Lista();
        ~Lista();
        bool existeElemento(int elemento);
        int umElemento(int linha);
        int posicao(int elemento);
        void insere(int elemento, int linha);
        void retira(int linha);
};

#endif // LISTA_H_INCLUDED
