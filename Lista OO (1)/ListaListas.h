#ifndef NODOLISTA_H
#define NODOLISTA_H


class nodoLista
{
    public:
        nodoLista(Lista* lista);
        virtual ~nodoLista();
        void setaLista(Lista* lista);
        void setaProxima(Lista* lista);
        Lista* pegaLista();
        Lista* pegaProxima();

    private:
        Lista* proxima;
};

#endif // LISTALISTAS_H
