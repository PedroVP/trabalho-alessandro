#include "Nodo.h"
#include <iostream>

        Nodo::Nodo(int elemento, int linha){
            this->elemento = elemento;
            this->proximo = NULL;
            this->linha = linha;
        }

        void Nodo::setaElemento(int elemento){
            this->elemento = elemento;
        }

        void Nodo::setaProximo(Nodo* nodo){
            this->proximo = nodo;
        }

        int Nodo::getLinha()
        {
            return linha;
        }

        int Nodo::pegaElemento(){
            return this->elemento;
        }

        Nodo* Nodo::pegaProximo(){
            return this->proximo;
        }
