#include <iostream>
#include "MatrizEsparsa.h"

using namespace std;

//!!!!!!!!!!!!!procurem coment�rios como esse no c�digo para encontrar �reas que podem precisar de ajustes!!!!!!!!!

//talvez tenham faltado checagens de NULL em alguns pontos onde se percorre ponteiros encadeados para garantir que o programa n�o crashe caso a posi��o especificada n�o "exista"

int main()
{
    int linhas, colunas, valor, opt, linha, coluna;
    linhas = 0;
    colunas = 0;
    opt = 0;

    do
    {
        cout << "Insira a quantidade de linhas: ";
        cin >> linhas;
    }while(linhas < 1);

    do
    {
        cout << "Insira a quantidade de colunas: ";
        cin >> colunas;
    }while(colunas < 1);

    MatrizEsparsa M(linhas,colunas);

    while(true)
    {
        do
        {
            cout << "(1) Adiciona elemento." << endl << "(2) Acrescenta valor � coluna." << endl << "(3) Remove elemento." << endl << "(4) Fecha programa." << endl;
            cin >> opt;
        }while(opt < 1 || opt > 4);

        valor = 0;
        coluna = 0;
        linha = 0;
        switch(opt)
        {
            case 1:

                do
                {
                    cout << "Digite a linha que deseja modificar: ";
                    cin >> linha;
                }while(linha < 1 || linha > linhas);
                do
                {
                    cout << "Digite a coluna que deseja modificar: ";
                    cin >> coluna;
                }while(coluna < 1 || coluna > colunas);

                cout << "Digite o valor que deseja inserir no local: ";
                cin >> valor;
                M.adicionaElemento(linha, coluna, valor);
                break;

            case 2:
                do
                {
                    cout << "Digite a coluna que deseja modificar: ";
                    cin >> coluna;
                }while(coluna < 1 || coluna > colunas);

                cout << "Digite o valor que deseja inserir no local: ";
                cin >> valor;
                M.adicionaAcoluna(coluna, valor);
                break;

            case 3:
                do
                {
                    cout << "Digite a linha que deseja modificar: ";
                    cin >> linha;
                }while(linha < 1 || linha > linhas);
                do
                {
                    cout << "Digite a coluna que deseja modificar: ";
                    cin >> coluna;
                }while(coluna < 1 || coluna > colunas);

                M.removeElemento(linha, coluna);
                break;

            case 4:
                return 0;
                break;

        }

        M.mostraMatriz();

    }

    return 0;
}
