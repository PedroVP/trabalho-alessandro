#include <iostream>
#include "nodoLista.h"
#include "Lista.cpp"

        NodoLista::NodoLista(int coluna)
        {
            atual = new Lista();
            proxima = NULL;
            this->coluna = coluna;
            acrescimoElementos = 0;
        }

        NodoLista::~NodoLista()
        {
            //dtor
        }

        int NodoLista::getColuna()
        {
            return coluna;
        }

        void NodoLista::acrescentaValor(int valor)
        {
            acrescimoElementos += valor;
        }

        void NodoLista::setaLista(Lista* lista)
        {
            atual = lista;
        }

        void NodoLista::setaProxima(NodoLista* nodolista)
        {
            proxima = nodolista;
        }

        Lista* NodoLista::pegaLista()
        {
            return atual;
        }

        NodoLista* NodoLista::pegaProxima()
        {
            return proxima;
        }

        void NodoLista::insere(int linha, int valor)
        {
            atual->insere(valor, linha);
        }

        void NodoLista::mostra(int linha)
        {
            if(atual)
            {
                cout << atual->umElemento(linha) + acrescimoElementos << " ";
            }
            else
            {
                cout << acrescimoElementos << " ";
            }
        }
